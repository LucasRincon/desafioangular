# Desafio-Angular

A aplicação consiste numa tela de perguntas para um jogo de tabuleiro.

Conforme o andamento no tabuleiro, é exibida a tela de perguntas.

As perguntas são divididas por nível(Tema).

O usuário só passa de ao acertar uma pergunta do tema nível atual.

Ao finalizar os niveis é exibido o resultado.

## Técnologias utilizadas

Angular 10
Bootstrap 4

## Instalação e execução

Crie um clone ou baixe um .zip da aplicação.

Importe a aplicação com sua IDE ou editor de preferencia. Após a importação execute a seguinte comando no terminal:
```sh
ng serve
```
obs.: Certifique-se de possuir o node.js instalado.

## Contato

Lucas Rincon
lucas_rincon26@hotmail.com
[Gitlab](https://gitlab.com/LucasRincon)