import { Question } from './questions';

export interface Object {
    theme: String,
    questions: Question[]
    question?: Question;
}