export interface Question {
    id: number;
    title: string;
    answers: {
        a: String;
        b: String;
        c: String;
        d: String;
    }
    solution: String;
    correctness?: boolean;
    answer?: String;
}