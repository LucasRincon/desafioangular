import { Object } from './object'

export const QUESTIONS: Object[] = [
  {
    theme: 'Tema 1',
    questions: [
      {
        id: 1,
        title: 'Pergunta 1',
        answers: {
          a: 'Resposta 1',
          b: 'Resposta 2',
          c: 'Resposta 3',
          d: 'Resposta 4',
        },
        solution: 'a',
      },
      {
        id: 2,
        title: 'Pergunta 2',
        answers: {
          a: 'Resposta 1',
          b: 'Resposta 2',
          c: 'Resposta 3',
          d: 'Resposta 4',
        },
        solution: 'b',
      },

      {
        id: 3,
        title: 'Pergunta 3',
        answers: {
          a: 'Resposta 1',
          b: 'Resposta 2',
          c: 'Resposta 3',
          d: 'Resposta 4',
        },
        solution: 'd',
      },
    ],
  },
  {
    theme: 'Tema 2',
    questions: [
      {
        id: 1,
        title: 'Pergunta 1',
        answers: {
          a: 'Resposta 1',
          b: 'Resposta 2',
          c: 'Resposta 3',
          d: 'Resposta 4',
        },
        solution: 'c',
      },
      {
        id: 2,
        title: 'Pergunta 2',
        answers: {
          a: 'Resposta 1',
          b: 'Resposta 2',
          c: 'Resposta 3',
          d: 'Resposta 4',
        },
        solution: 'b',
      },

      {
        id: 3,
        title: 'Pergunta 3',
        answers: {
          a: 'Resposta 1',
          b: 'Resposta 2',
          c: 'Resposta 3',
          d: 'Resposta 4',
        },
        solution: 'd',
      },
    ],
  },
];
