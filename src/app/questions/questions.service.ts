import { Question } from './questions'
import { QUESTIONS } from './mock-questions'
import { Object } from './object'
import { Router } from '@angular/router'
import { Injectable } from '@angular/core'

@Injectable()
export class QuestionsService {

    private displayedQuestions: Question[] = []

    private atual: Object;

    private indexTheme: number = 0

    private indexQuestion: number = 0
  
    constructor(
        private router: Router,
    ) { }
  
    getQuestion(): Object {
        if(this.indexTheme >= QUESTIONS.length){
            this.router.navigateByUrl("/results")
        }
        if(this.indexTheme < QUESTIONS.length && this.indexQuestion >= QUESTIONS[this.indexTheme].questions.length){
            this.indexQuestion = 0
            this.indexTheme +=1
        }
        this.atual = {theme: QUESTIONS[this.indexTheme].theme, questions: [], question: QUESTIONS[this.indexTheme].questions[this.indexQuestion]}
        this.indexQuestion += 1
        return this.atual
    }

    postAnswer(answer: String) {
        var correctness = answer === this.atual.question.solution
        this.displayedQuestions.push({...this.atual.question, correctness, answer })
        if(correctness){
            this.indexTheme++
            this.indexQuestion = 0
        }
    }

    getLevel(): number {
        return this.indexTheme+1
    }
}