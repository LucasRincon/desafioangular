import { Component, OnInit } from '@angular/core';
import { QuestionsService } from './questions.service'
import { Object } from './object';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss',],
  providers: [QuestionsService]
})
export class QuestionsComponent implements OnInit {

  data: Object;
  
  answer: String;

  level: number;

  constructor(private service: QuestionsService) { }

  ngOnInit(): void {
    this.data = this.service.getQuestion()
    this.level = this.service.getLevel()
  }

  onItemChange(value: String) {
    this.answer = value
  }

  submit() {
    this.service.postAnswer(this.answer)
    this.data = this.service.getQuestion()
    this.level = this.service.getLevel()
  }
}
