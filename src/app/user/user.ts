export interface User {
    userId: number;
    username: String;
    photo: String;
    score: number;
}