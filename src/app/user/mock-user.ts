import { User } from './user'

export const USER: User = {
    userId: 1,
    username: "Username",
    photo: "../../assets/user.png",
    score: 30
}
