import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service'
import { User } from './user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  providers: [UserService]
})
export class UserComponent implements OnInit {

  user: User;

  constructor(private service: UserService) { }

  ngOnInit(): void {
    this.user = this.service.getUser()
  }

}
