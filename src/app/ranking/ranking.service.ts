import { User } from '../user/user'
import { RANKING } from './mock-ranking'

export class RankingService {

    getRanking(): User[]{
        return RANKING;
    }
}
