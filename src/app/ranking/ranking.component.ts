import { Component, OnInit } from '@angular/core'
import { RankingService } from './ranking.service'
import { User } from '../user/user'

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss'],
  providers: [RankingService]
})
export class RankingComponent implements OnInit {

  ranking: User[];

  constructor(private service: RankingService) { }

  ngOnInit(): void {
    this.onLoad()
  }

  onLoad() {
    var rank = this.service.getRanking()
    this.ranking = rank.sort((a, b) => {return b.score - a.score});
    console.log(this.ranking)
  }
}
