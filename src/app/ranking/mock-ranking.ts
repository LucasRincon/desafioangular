import { User } from '../user/user';

export const RANKING: User[] = [
  {
    userId: 1,
    username: 'Username',
    photo: '../../assets/user.png',
    score: 100,
  },
  {
    userId: 2,
    username: 'Username',
    photo: '../../assets/user.png',
    score: 50,
  },
  {
    userId: 3,
    username: 'Username',
    photo: '../../assets/user.png',
    score: 70,
  },
  {
    userId: 4,
    username: 'Username',
    photo: '../../assets/user.png',
    score: 30,
  },
  {
    userId: 5,
    username: 'Username',
    photo: '../../assets/user.png',
    score: 40,
  },
];
