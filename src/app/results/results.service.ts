import { Result } from './result'
import { RESULTS } from './mock-results';

export class ResultsService {
    getResult(): Result[] {
        return RESULTS;
    }
}