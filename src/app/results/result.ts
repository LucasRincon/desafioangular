import { Answers } from "./answers";

export interface Result {
    theme: String;
    answers: Answers[];
}