export interface Answers {
    questionId: number;
    answer: String;
    yourAnswer: String;
    result: boolean;
}