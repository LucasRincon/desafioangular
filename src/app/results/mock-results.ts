import { Result } from './result'

export const RESULTS: Result[] = [
    {
    theme: "Tema 1",
    answers: [
            {
                questionId: 1,
                answer: "a",
                yourAnswer: "b",
                result: false
            },
            {
                questionId: 2,
                answer: "b",
                yourAnswer: "c",
                result: false
            },
            {
                questionId: 3,
                answer: "c",
                yourAnswer: "c",
                result: true
            }
        ]
    },
    {
        theme: "Tema 2",
        answers: [
                {
                    questionId: 1,
                    answer: "a",
                    yourAnswer: "b",
                    result: false
                },
                {
                    questionId: 2,
                    answer: "a",
                    yourAnswer: "b",
                    result: true
                },
                {
                    questionId: 3,
                    answer: "c",
                    yourAnswer: "a",
                    result: true
                }
            ]
        }
]