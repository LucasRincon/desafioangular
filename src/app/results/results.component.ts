import { Component, OnInit } from '@angular/core';
import { ResultsService } from './results.service'
import { Result } from './result';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
  providers: [ResultsService]
})
export class ResultsComponent implements OnInit {

  data: Result[];

  table: String;

  constructor(private service: ResultsService) { }

  ngOnInit(): void {
    this.data = this.service.getResult()
  }
}
